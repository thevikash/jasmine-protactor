import { AppPage } from "./app.po";
import { browser, logging, element, WebDriver } from "protractor";

describe("workspace-project App", () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  it("should display correct text on login page", () => {
    page.navigateTo();
    expect(page.getUserNameLabel()).toEqual("Username");
    expect(page.getPasswordLabel()).toEqual("Password");
    expect(page.getLoginButtonText()).toEqual("Login");
  });

  it("should show username required if login clicked before filling it", async (): Promise<
    void
  > => {
    page.navigateTo();
    page.getPasswordField().sendKeys("1234");
    await page.getLoginButton().click();
    browser.wait(page.getUserNameError(), 1000);
    expect(page.getUserNameError()).toEqual("username is required");
  });

  it("should show password required if login clicked before filling it", async (): Promise<
    void
  > => {
    page.navigateTo();
    page.getUserNameField().sendKeys("admin");
    await page.getLoginButton().click();
    expect(page.getPasswordError()).toEqual("password is required");
  });

  it("should show error if wrong credential entered", async (): Promise<
    void
  > => {
    page.navigateTo();
    page.getUserNameField().sendKeys("admin");
    page.getPasswordField().sendKeys("1234");
    await page.getLoginButton().click();
    expect(page.getLoginError()).toEqual("username or password invalid");
  });

  it("should navigate to dashboard page if credentials are correct", async (): Promise<
    void
  > => {
    page.navigateTo();
    page.getUserNameField().sendKeys("admin");
    page.getPasswordField().sendKeys("admin");
    await page.getLoginButton().click();
    browser.wait(page.getDashboardTitle(), 1000);
    expect(page.getDashboardTitle()).toEqual("Dashboard Page");
  });

  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    browser.sleep(1000);
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);
    expect(logs).not.toContain(
      jasmine.objectContaining({
        level: logging.Level.SEVERE,
      } as logging.Entry)
    );
  });
});
