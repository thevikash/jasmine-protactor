import { browser, by, element } from "protractor";

export class AppPage {
  navigateTo() {
    return browser.get(browser.baseUrl) as Promise<any>;
  }

  getUserNameLabel() {
    return element(by.css("#username-label")).getText() as Promise<string>;
  }

  getPasswordLabel() {
    return element(by.css("#password-label")).getText() as Promise<string>;
  }

  getLoginButtonText() {
    return element(by.css("#login-button")).getText() as Promise<string>;
  }

  getUserNameField() {
    return element(by.css("#username-textbox"));
  }

  getPasswordField() {
    return element(by.css("#password-textbox"));
  }

  getLoginButton() {
    return element(by.css("#login-button"));
  }

  getUserNameError() {
    return (
      element(by.css("#username-error")).getText() || (null as Promise<string>)
    );
  }

  getPasswordError() {
    return (
      element(by.css("#password-error")).getText() || (null as Promise<string>)
    );
  }

  getLoginError() {
    return (
      element(by.css("#login-error")).getText() || (null as Promise<string>)
    );
  }

  getDashboardTitle() {
    return (
      element(by.css("#dashboard-page")).getText() || (null as Promise<string>)
    );
  }
}
