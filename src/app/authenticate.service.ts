import { Injectable } from "@angular/core";
import { Observable, Observer } from "rxjs";

const DB: Array<{ username: string; password: string }> = [
  {
    username: "admin",
    password: "admin",
  },
];

@Injectable({
  providedIn: "root",
})
export class AuthenticateService {
  private _isAuthenticated = false;

  public get isAuthenticated(): boolean {
    return this._isAuthenticated;
  }
  constructor() {}

  public authenticate(username, password): Observable<boolean> {
    return new Observable((observer: Observer<boolean>) => {
      this._isAuthenticated =
        DB.filter((e) => e.username === username && e.password === password)
          .length !== 0;
      observer.next(this._isAuthenticated);
      observer.complete();
    });
  }
}
