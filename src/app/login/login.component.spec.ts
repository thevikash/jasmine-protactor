import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { LoginComponent } from "./login.component";
import { ReactiveFormsModule } from "@angular/forms";
import { RouterTestingModule } from "@angular/router/testing";
import { AuthenticateService } from "../authenticate.service";
import { of } from "rxjs";
import { Router } from "@angular/router";

describe("LoginComponent", () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;
  let authenticateService: AuthenticateService;
  let router: Router;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [LoginComponent],
      imports: [ReactiveFormsModule, RouterTestingModule],
      providers: [AuthenticateService],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    authenticateService = TestBed.get(AuthenticateService);
    router = TestBed.get(Router);
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });

  it("should not call authenticate on login event if userName or password is not filled", (): void => {
    spyOn(authenticateService, "authenticate");
    component.login();
    expect(authenticateService.authenticate).not.toHaveBeenCalled();
  });

  it("should call authenticate if credentials are filled and show error if authentication failed", () => {
    spyOn(authenticateService, "authenticate").and.returnValue(of(false));
    component.userName.setValue("Vikash");
    component.password.setValue("1234");
    component.login();
    expect(component.loginForm.errors.invalid).toBeTruthy();
  });

  it("should call authenticate method for valid credentials and navigate to dashboard", () => {
    spyOn(authenticateService, "authenticate").and.returnValue(of(true));
    spyOn(router, "navigate").and.returnValue(null);
    component.userName.setValue("Vikash");
    component.password.setValue("1234");
    component.login();
    expect(component.loginForm.errors).toEqual(null);
    expect(router.navigate).toHaveBeenCalled();
  });
});
