import { Component, OnInit } from "@angular/core";
import { FormControl, Validators, FormGroup } from "@angular/forms";
import { AuthenticateService } from "../authenticate.service";
import { Router } from "@angular/router";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.scss"],
})
export class LoginComponent implements OnInit {
  public userName: FormControl = new FormControl(null, Validators.required);
  public password: FormControl = new FormControl(null, Validators.required);

  public loginForm: FormGroup = new FormGroup({
    userName: this.userName,
    password: this.password,
  });

  constructor(
    private authenticateService: AuthenticateService,
    private router: Router
  ) {}

  ngOnInit() {}

  public login(): void {
    this.loginForm.markAllAsTouched();
    this.loginForm.setErrors(null);
    if (this.loginForm.valid) {
      this.authenticateService
        .authenticate(this.userName.value, this.password.value)
        .subscribe((isValid) => {
          if (isValid) {
            this.router.navigate(["/dashboard"]);
          } else {
            this.loginForm.setErrors({ invalid: true });
          }
        });
    }
  }
}
